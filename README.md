# Docker Compose Laravel Project
This project demonstrates the use of docker (using docker-compose) in developing a very basic Laravel project. 


The commands used to create the Laravel Project:
```bash
  docker run --rm -v "$(pwd)":/app composer/composer:alpine create-project --prefer-dist laravel/laravel code
  cd code
  docker run --rm -v "$(pwd)":/app composer/composer:alpine install
  cp .env.example .env
  docker-compose exec app php artisan key:generate
```

To start the entire system use ```docker-compose up``` or ```docker-compose up -d``` to run everything in the background. Once running, visit ```http://localhost:8080``` to see your site.


Some additional commands showing how to run commands inside of a docker container that is active:
```bash
docker-compose exec app php artisan migrate --seed
docker-compose exec app php artisan make:controller MyController
```

